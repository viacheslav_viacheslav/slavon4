package com.slavon.dao;

import com.slavon.entity.Specialization;

import java.util.List;

public interface SpecializationDao {

	void insertOrUpdateSpecialization(Specialization specialization);

	Specialization getSpecializationById(int id);

	void deleteSpecialization(Specialization specialization);

	List<Specialization> getAllSpecialization();
}
