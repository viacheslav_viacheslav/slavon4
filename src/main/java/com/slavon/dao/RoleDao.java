package com.slavon.dao;

import com.slavon.entity.Role;

import java.util.List;

public interface RoleDao {
    void insertOrUpdateRole(Role role);

    Role getRoleById(int id);

    void deleteRole(Role role);

    List<Role> getAllRoles();
}
