package com.slavon.dao;

import com.slavon.entity.Car;

import java.util.List;

public interface CarDao {

    void insertOrUpdateCar(Car car);

    Car getCarById(int id);

    void deleteCar(Car car);

    List<Car> getAllCars();
}
