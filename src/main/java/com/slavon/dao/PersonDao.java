package com.slavon.dao;

import com.slavon.entity.Person;

import java.util.List;

public interface PersonDao {

	void insertOrUpdatePerson(Person person);

	Person getPersonById(int id);

	Person getPersonByLogin(String name);

	void deletePerson(Person person);

	List<Person> getAllPeople();

	List<Person> getClients();

	List<Person> getRepairers();
}
