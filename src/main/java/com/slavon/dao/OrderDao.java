package com.slavon.dao;

import com.slavon.entity.Order;

import java.util.List;

public interface OrderDao {
	
	void insertOrUpdateOrder(Order order);

	Order getOrderById(int id);

	void deleteOrder(Order order);

	List<Order> getAllOrders();
}
