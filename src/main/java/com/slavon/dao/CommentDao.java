package com.slavon.dao;

import com.slavon.entity.Comment;

import java.util.List;

public interface CommentDao {

    int insertComment(Comment comment);

    Comment getCommentById(int id);

    List<Comment> getAllComments();
}
