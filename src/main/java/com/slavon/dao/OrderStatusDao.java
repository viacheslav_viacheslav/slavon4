package com.slavon.dao;

import com.slavon.entity.OrderStatus;

import java.util.List;

public interface OrderStatusDao {

    void insertOrUpdateOrderStatus(OrderStatus orderStatus);

    OrderStatus getOrderStatusById(int id);

    void deleteOrderStatus(OrderStatus orderStatus);

    List<OrderStatus> getAllOrderStatuses();
}
