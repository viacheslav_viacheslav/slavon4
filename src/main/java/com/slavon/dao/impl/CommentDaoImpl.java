package com.slavon.dao.impl;

import com.slavon.entity.Comment;
import com.slavon.dao.CommentDao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("commentDao")
public class CommentDaoImpl implements CommentDao {

	private SessionFactory sessionFactory;

	public CommentDaoImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public int insertComment(Comment comment) {
		Session session = sessionFactory.getCurrentSession();
		return (int) session.save(comment);
	}

	@Override
	public Comment getCommentById(int id) {
		Session session = sessionFactory.getCurrentSession();
		return (Comment) session.get(Comment.class, new Integer(id));
	}

	@Override
	public List<Comment> getAllComments() {
		Session session = sessionFactory.getCurrentSession();
		return session.createQuery("from COMMENT").list();
	}
}
