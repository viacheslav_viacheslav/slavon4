package com.slavon.dao.impl;

import com.slavon.entity.Specialization;
import com.slavon.dao.SpecializationDao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("specializationDao")
public class SpecializationDaoImpl implements SpecializationDao {

	private SessionFactory sessionFactory;

	public SpecializationDaoImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void insertOrUpdateSpecialization(Specialization specialization) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(specialization);
	}

	@Override
	public Specialization getSpecializationById(int id) {
		Session session = sessionFactory.getCurrentSession();
		return (Specialization) session.get(Specialization.class, new Integer(id));
	}
	
	@Override
	public void deleteSpecialization(Specialization specialization) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(specialization);
	}

	@Override
	public List<Specialization> getAllSpecialization() {
		Session session = sessionFactory.getCurrentSession();
		return session.createQuery("from SPECIALIZATION").list();
	}
}
