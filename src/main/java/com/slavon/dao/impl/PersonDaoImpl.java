package com.slavon.dao.impl;

import com.slavon.entity.Person;
import com.slavon.dao.PersonDao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("personDao")
@Transactional
public class PersonDaoImpl implements PersonDao {

    private SessionFactory sessionFactory;

    public PersonDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void insertOrUpdatePerson(Person person) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(person);
    }

    @Override
    public Person getPersonById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return (Person) session.get(Person.class, new Integer(id));
    }

    @Override
    public Person getPersonByLogin(String login) {
        Session session = sessionFactory.getCurrentSession();
        Criteria crit = session.createCriteria(Person.class);
        crit.add(Restrictions.eq("login", login));
        return (Person) crit.uniqueResult();
    }

    @Override
    public void deletePerson(Person person) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(person);
    }

    @Override
    public List<Person> getAllPeople() {
        Session session = sessionFactory.getCurrentSession();
        List<Person> personList = session.createQuery("from PERSON as person " +
                "where person.status <> 'INACTIVE' or person.status IS NULL").list();
        return personList;
    }

    @Override
    public List<Person> getClients() {
        Session session = sessionFactory.getCurrentSession();
        List<Person> clientsList = session.createQuery("from PERSON as person " +
                "where (person.status != 'INACTIVE' or person.status IS NULL) and person.role = 1").list();
        return clientsList;
    }

    public List<Person> getRepairers() {
        Session session = sessionFactory.getCurrentSession();
        List<Person> repairersList = session.createQuery("from PERSON as person " +
                "where (person.status != 'INACTIVE' or person.status IS NULL) and person.role = 2").list();
        return repairersList;
    }

}
