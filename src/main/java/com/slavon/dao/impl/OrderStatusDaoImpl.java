package com.slavon.dao.impl;

import com.slavon.entity.OrderStatus;
import com.slavon.dao.OrderStatusDao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("orderStatusDao")
public class OrderStatusDaoImpl implements OrderStatusDao {

    private SessionFactory sessionFactory;

    public OrderStatusDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void insertOrUpdateOrderStatus(OrderStatus orderStatus) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(orderStatus);
    }

    @Override
    public OrderStatus getOrderStatusById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return (OrderStatus) session.get(OrderStatus.class, new Integer(id));
    }

    @Override
    public void deleteOrderStatus(OrderStatus orderStatus) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(orderStatus);
    }

    @Override
    public List<OrderStatus> getAllOrderStatuses() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from ORDER_STATUS").list();
    }
}
