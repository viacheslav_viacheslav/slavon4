package com.slavon.dao.impl;

import com.slavon.entity.Car;
import com.slavon.dao.CarDao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("carDao")
public class CarDaoImpl implements CarDao {

    private SessionFactory sessionFactory;

    public CarDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void insertOrUpdateCar(Car car) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(car);
    }

    @Override
    public Car getCarById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return (Car) session.get(Car.class, new Integer(id));
    }

    @Override
    public void deleteCar(Car car) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(car);
    }

    @Override
    public List<Car> getAllCars() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from CAR").list();
    }
}
