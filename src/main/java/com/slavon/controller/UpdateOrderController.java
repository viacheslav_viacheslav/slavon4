package com.slavon.controller;

import com.slavon.entity.Order;
import com.slavon.service.OrderService;
import com.slavon.entity.Person;
import com.slavon.service.PersonService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


@ManagedBean(name = "updateOrderController")
@SessionScoped
public class UpdateOrderController {

    @ManagedProperty(value = "#{personService}")
    private PersonService personService;

    @ManagedProperty(value = "#{orderService}")
    private OrderService orderService;

    private Person client;

    private Person repairer;

    private Order order;

    private Order targetOrder;

    public String editAction(Order order) {
        targetOrder = order;
        targetOrder.getLastComment().setCommentText("");
        return "/pages/updateOrderPage.xhtml?faces-redirect=true";
    }

    public String confirmUpdateOrder() {
        this.targetOrder.getLastComment().setPerson(detectCurrentUser());
        orderService.insertOrUpdateOrder(this.targetOrder);
        return "/pages/orderListPage?faces-redirect=true";
    }

    public Person detectCurrentUser() {
        String login = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
        return personService.getPersonByLogin(login);
    }

    public String cancelUpdateAction() {
        return "/pages/orderListPage?faces-redirect=true";
    }

    public Person getClient() {
        return client;
    }

    public void setClient(Person client) {
        this.client = client;
    }

    public Person getRepairer() {
        return repairer;
    }

    public void setRepairer(Person repairer) {
        this.repairer = repairer;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public PersonService getPersonService() {
        return personService;
    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    public OrderService getOrderService() {
        return orderService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public Order getTargetOrder() {
        return targetOrder;
    }

    public void setTargetOrder(Order targetOrder) {
        this.targetOrder = targetOrder;
    }
}
