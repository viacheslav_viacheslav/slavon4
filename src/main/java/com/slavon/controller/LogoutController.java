package com.slavon.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="logoutController")
@SessionScoped
public class LogoutController {

	public String logout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/index.xhtml?faces-redirect=true";
	}

	public  String home(){
		return "/pages/adminpage.xhtml?faces-redirect=true";
	}
}
