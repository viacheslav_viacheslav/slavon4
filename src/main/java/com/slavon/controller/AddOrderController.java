package com.slavon.controller;

import com.slavon.entity.Car;
import com.slavon.entity.Comment;
import com.slavon.entity.Order;
import com.slavon.entity.OrderStatus;
import com.slavon.service.OrderService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;


@ManagedBean(name = "addOrderController")
@ViewScoped
public class AddOrderController {

    @ManagedProperty(value = "#{orderService}")
    private OrderService orderService;

    @ManagedProperty(value = "#{updateOrderController}")
    private UpdateOrderController updateOrderController;

    private Order targetOrder;

    @PostConstruct
    public void init() {
        Order order = new Order();
        order.setLastComment(new Comment());
        order.setCar(new Car());
        order.setOrderStatus(new OrderStatus());
        this.targetOrder = order;
    }

    public String confirmAddOrder() {
        this.targetOrder.getLastComment().setPerson(updateOrderController.detectCurrentUser());
        orderService.insertOrUpdateOrder(this.targetOrder);
        return "/pages/orderListPage?faces-redirect=true";
    }

    public OrderService getOrderService() {
        return orderService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public UpdateOrderController getUpdateOrderController() {
        return updateOrderController;
    }

    public void setUpdateOrderController(UpdateOrderController updateOrderController) {
        this.updateOrderController = updateOrderController;
    }

    public Order getTargetOrder() {
        return targetOrder;
    }

    public void setTargetOrder(Order targetOrder) {
        this.targetOrder = targetOrder;
    }
}
