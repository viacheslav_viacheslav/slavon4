package com.slavon.controller;

import com.slavon.entity.Car;
import com.slavon.entity.Order;
import com.slavon.entity.OrderStatus;
import com.slavon.entity.Person;
import com.slavon.service.CarService;
import com.slavon.service.CommentService;
import com.slavon.service.OrderService;
import com.slavon.service.OrderStatusService;
import com.slavon.service.PersonService;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;


@ManagedBean(name = "orderListController")
@ViewScoped
public class OrderListController {

    @ManagedProperty(value = "#{personService}")
    private PersonService personService;

    @ManagedProperty(value = "#{carService}")
    private CarService carService;

    @ManagedProperty(value = "#{orderStatusService}")
    private OrderStatusService orderStatusService;

    @ManagedProperty(value = "#{commentService}")
    private CommentService commentService;

    @ManagedProperty(value = "#{orderService}")
    private OrderService orderService;

    private List<Order> orders;

    private Person client;

    private Person repairer;

    private Order order;

    private List<Car> cars;

    private List<Person> clients;

    private List<OrderStatus> orderStatuses;

    private List<Person> repairers;

    public List<Order> getOrders() {
        return orderService.getAllOrders();
    }

    public String addOrder() {
        return "/pages/addOrderPage.xhtml?faces-redirect=true";
    }

    public Person getClient(int id) {
        return personService.getPersonById(id);
    }

    public Person getRepairer(int id) {
        return personService.getPersonById(id);
    }

    public List<Car> getCars() {
        return carService.getAllCars();
    }

    public List<Person> getClients() {
        return personService.getClients();
    }

    public List<OrderStatus> getOrderStatuses() {
        return orderStatusService.getAllOrderStatuses();
    }

    public List<Person> getRepairers() {
        return personService.getRepairers();
    }

    public PersonService getPersonService() {
        return personService;
    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    public CarService getCarService() {
        return carService;
    }

    public void setCarService(CarService carService) {
        this.carService = carService;
    }

    public OrderStatusService getOrderStatusService() {
        return orderStatusService;
    }

    public void setOrderStatusService(OrderStatusService orderStatusService) {
        this.orderStatusService = orderStatusService;
    }

    public CommentService getCommentService() {
        return commentService;
    }

    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    public OrderService getOrderService() {
        return orderService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }
}
