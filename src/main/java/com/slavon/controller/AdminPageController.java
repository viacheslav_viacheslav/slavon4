package com.slavon.controller;

import com.slavon.entity.Person;
import com.slavon.service.PersonService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;


@ManagedBean(name = "adminPageController")
@RequestScoped
public class AdminPageController {

    @ManagedProperty("#{personService}")
    private PersonService personService;

    public Person getUser() {
        String userLogin = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
            return personService.getPersonByLogin(userLogin);
    }

    public String navigateToOrderList() {
        return "/pages/orderListPage?faces-redirect=true";
    }

    public String navigateToRepairerList() {
        return "/pages/repairerListPage?faces-redirect=true";
    }

    public String navigateToUserList() {
        return "/pages/userListPage?faces-redirect=true";
    }

    public PersonService getPersonService() {
        return personService;
    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }
}
