package com.slavon.controller;

import com.slavon.entity.Person;
import com.slavon.service.PersonService;
import com.slavon.service.SpecializationService;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;


@ManagedBean(name = "repairerListController")
@RequestScoped
public class RepairerListController {

    @ManagedProperty(value = "#{personService}")
    private PersonService personService;

    @ManagedProperty(value = "#{specializationService}")
    private SpecializationService specializationService;

    private List<Person> repairers;

    public List<Person> getRepairers() {
        return personService.getRepairers();
    }

    public PersonService getPersonService() {
        return personService;
    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    public SpecializationService getSpecializationService() {
        return specializationService;
    }

    public void setSpecializationService(SpecializationService specializationService) {
        this.specializationService = specializationService;
    }
}
