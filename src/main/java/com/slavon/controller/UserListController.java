package com.slavon.controller;

import com.slavon.entity.Person;
import com.slavon.service.PersonService;
import com.slavon.entity.Role;
import com.slavon.service.RoleService;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;


@ManagedBean(name = "userListController")
@ViewScoped
public class UserListController {

    @ManagedProperty(value = "#{personService}")
    private PersonService personService;

    @ManagedProperty(value = "#{roleService}")
    private RoleService roleService;

    private List<Person> users;

    private Person userToUpdate;

    private List<Role> roles;

    public void updatePerson(Person person) {
        userToUpdate = person;
    }

    public void confirmUpdatePerson() {
        personService.insertOrUpdatePerson(userToUpdate);
    }

    public void addPerson() {
        Person person = new Person();
        person.setPersonRole(new Role());
        userToUpdate = person;
    }

    public void confirmDeletePerson(Person person) {
        personService.deletePerson(person);
    }

    public Person getUserToUpdate() {
        return userToUpdate;
    }

    public void setUserToUpdate(Person person) {
        this.userToUpdate = person;
    }

    public List<Role> getRoles() {
        return roleService.getAllRoles();
    }

    public List<Person> getUsers() {
        return personService.getAllPeople();
    }

    public PersonService getPersonService() {
        return personService;
    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    public RoleService getRoleService() {
        return roleService;
    }

    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }
}
