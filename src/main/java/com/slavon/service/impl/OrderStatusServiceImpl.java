package com.slavon.service.impl;

import com.slavon.dao.OrderStatusDao;
import com.slavon.entity.OrderStatus;
import com.slavon.service.OrderStatusService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("orderStatusService")
@Transactional
public class OrderStatusServiceImpl implements OrderStatusService {

    @Autowired
    private OrderStatusDao orderStatusDao;

    public OrderStatusDao getOrderStatusDao() {
        return orderStatusDao;
    }

    public void setOrderStatusDao(OrderStatusDao orderStatusDao) {
        this.orderStatusDao = orderStatusDao;
    }

    @Override
    public void insertOrUpdateOrderStatus(OrderStatus orderStatus) {
        orderStatusDao.insertOrUpdateOrderStatus(orderStatus);
    }

    @Override
    public OrderStatus getOrderStatusById(int id) {
        return orderStatusDao.getOrderStatusById(id);
    }

    @Override
    public void deleteOrderStatus(OrderStatus orderStatus) {
        orderStatusDao.deleteOrderStatus(orderStatus);
    }

    @Override
    public List<OrderStatus> getAllOrderStatuses() {
        return orderStatusDao.getAllOrderStatuses();
    }

    @Override
    public Map<Integer, OrderStatus> gerOrderStatusMap() {
        List<OrderStatus> orderStatusList = orderStatusDao.getAllOrderStatuses();
        return orderStatusList.stream().collect(Collectors.toMap(OrderStatus::getOrderStatusId, orderStatus -> orderStatus));
    }
}
