package com.slavon.service.impl;

import com.slavon.dao.PersonDao;
import com.slavon.entity.Person;
import com.slavon.service.PersonService;
import com.slavon.service.RoleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("personService")
@Transactional
public class PersonServiceImpl implements PersonService {

    @Autowired
    private RoleService roleService;

    public RoleService getRoleService() {
        return roleService;
    }

    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @Autowired
    private PersonDao personDao;

    public PersonDao getPersonDao() {
        return personDao;
    }

    public void setPersonDao(PersonDao personDao) {
        this.personDao = personDao;
    }

    @Override
    public void insertOrUpdatePerson(Person person) {
        personDao.insertOrUpdatePerson(person);
    }

    @Override
    public Person getPersonById(int id) {
        return personDao.getPersonById(id);
    }

    @Override
    public Person getPersonByLogin(String login) {
        return personDao.getPersonByLogin(login);
    }

    @Override
    public void deletePerson(Person person) {
        person.setPersonStatus("INACTIVE");
        personDao.insertOrUpdatePerson(person);
    }

    @Override
    public List<Person> getAllPeople() {
        return personDao.getAllPeople();
    }

    @Override
    public Map<Integer, Person> getPersonMap() {
        List<Person> personList = getAllPeople();
        return personList.stream().collect(Collectors.toMap(Person::getPersonId, person -> person));
    }

    @Override
    public List<Person> getClients() {
        return personDao.getClients();
    }

    @Override
    public List<Person> getRepairers() {
        return personDao.getRepairers();
    }
}
