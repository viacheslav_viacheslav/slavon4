package com.slavon.service.impl;

import com.slavon.dao.SpecializationDao;
import com.slavon.entity.Specialization;
import com.slavon.service.SpecializationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("specializationService")
@Transactional
public class SpecializationServiceImpl implements SpecializationService {

    @Autowired
    private SpecializationDao specializationDao;

    public SpecializationDao getSpecializationDao() {
        return specializationDao;
    }

    public void setSpecializationDao(SpecializationDao specializationDao) {
        this.specializationDao = specializationDao;
    }

    @Override
    public void insertOrUpdateSpecialization(Specialization specialization) {
        specializationDao.insertOrUpdateSpecialization(specialization);
    }

    @Override
    public Specialization getSpecializationById(int id) {
        return specializationDao.getSpecializationById(id);
    }

    @Override
    public void deleteSpecialization(Specialization specialization) {
        specializationDao.deleteSpecialization(specialization);
    }

    @Override
    public List<Specialization> getAllSpecialization() {
        return specializationDao.getAllSpecialization();
    }
}
