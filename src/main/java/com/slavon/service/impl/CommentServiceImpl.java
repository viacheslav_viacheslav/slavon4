package com.slavon.service.impl;

import com.slavon.dao.CommentDao;
import com.slavon.entity.Comment;
import com.slavon.service.CommentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("commentService")
@Transactional
public class CommentServiceImpl implements CommentService{

	@Autowired
	private CommentDao commentDao;

	public CommentDao getCommentDao() {
		return commentDao;
	}

	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}

	@Override
	public void insertComment(Comment comment) {
		commentDao.insertComment(comment);
	}

	@Override
	public Comment getCommentById(int id) {
		return commentDao.getCommentById(id);
	}

	@Override
	public List<Comment> getAllComments() {
		return commentDao.getAllComments();
	}

	@Override
	public Map<Integer, Comment> getCommentMap(){
		List<Comment> allCommentsList = commentDao.getAllComments();
		return allCommentsList.stream().collect(Collectors.toMap(Comment::getCommentId, comment -> comment));
	}
}
