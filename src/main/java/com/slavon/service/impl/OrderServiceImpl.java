package com.slavon.service.impl;

import com.slavon.dao.CommentDao;
import com.slavon.dao.OrderDao;
import com.slavon.dao.PersonDao;
import com.slavon.entity.Comment;
import com.slavon.entity.Order;
import com.slavon.entity.Person;
import com.slavon.service.CarService;
import com.slavon.service.CommentService;
import com.slavon.service.OrderService;
import com.slavon.service.OrderStatusService;
import com.slavon.service.PersonService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service("orderService")
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    public OrderDao getOrderDao() {
        return orderDao;
    }

    public void setOrderDao(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Autowired
    private PersonDao personDao;

    public PersonDao getPersonDao() {
        return personDao;
    }

    public void setPersonDao(PersonDao personDao) {
        this.personDao = personDao;
    }

    @Autowired
    private CommentDao commentDao;

    public CommentDao getCommentDao() {
        return commentDao;
    }

    public void setCommentDao(CommentDao commentDao) {
        this.commentDao = commentDao;
    }

    @Autowired
    private PersonService personService;

    public PersonService getPersonService() {
        return personService;
    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    @Autowired
    private CarService carService;

    public CarService getCarService() {
        return carService;
    }

    public void setCarService(CarService carService) {
        this.carService = carService;
    }

    @Autowired
    private OrderStatusService orderStatusService;

    public OrderStatusService getOrderStatusService() {
        return orderStatusService;
    }

    public void setOrderStatusService(OrderStatusService orderStatusService) {
        this.orderStatusService = orderStatusService;
    }

    @Autowired
    private CommentService commentService;

    public CommentService getCommentService() {
        return commentService;
    }

    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    @Override
    public void insertOrUpdateOrder(Order order) {
        Set<Person> people = new HashSet<>();
        int clientId = order.getClientId();
        int repairerId = order.getRepairerId();
        if (clientId != 0) {
            people.add(personDao.getPersonById(clientId));
        }
        if (repairerId != 0) {
            people.add(personDao.getPersonById(repairerId));
        }
        if (people.size() != 0) {
            order.setPersonSet(people);
        }
        Comment comment = order.getLastComment();
        int commentId = commentDao.insertComment(comment);
        order.setLastComment(commentDao.getCommentById(commentId));
        orderDao.insertOrUpdateOrder(order);
    }

    @Override
    public void deleteOrder(Order order) {
        orderDao.deleteOrder(order);
    }

    @Override
    public List<Order> getAllOrders() {
        List<Order> orderList = orderDao.getAllOrders();
        return orderList.stream().map(order -> setPeopleForOrder(order, order.getPersonSet())).collect(Collectors.toList());
    }

    @Override
    public Order setPeopleForOrder(Order order, Set<Person> personSet) {
        for (Person person : personSet) {
            int persId = person.getPersonId();
            if ((persId != 0) && "ROLE_CLIENT".equals(getPersonRole(persId))) {
                order.setClientId(persId);
            } else if ((persId != 0) && "ROLE_REPAIRER".equals(getPersonRole(persId))) {
                order.setRepairerId(persId);
            }
        }
        return order;
    }

    private String getPersonRole(int personId) {
        return personDao.getPersonById(personId).getPersonRole().getRoleName();
    }
}
