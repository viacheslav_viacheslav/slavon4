package com.slavon.service.impl;

import com.slavon.dao.CarDao;
import com.slavon.entity.Car;
import com.slavon.service.CarService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("carService")
@Transactional
public class CarServiceImpl implements CarService{

	@Autowired
	private CarDao carDao;

	public CarDao getCarDao() {
		return carDao;
	}

	public void setCarDao(CarDao carDao) {
		this.carDao = carDao;
	}

	@Override
	public void insertOrUpdateCar(Car car) {
		carDao.insertOrUpdateCar(car);
	}

	@Override
	public Car getCarById(int id) {
		return carDao.getCarById(id);
	}

	@Override
	public void deleteCar(Car car) {
		carDao.deleteCar(car);
	}

	@Override
	public List<Car> getAllCars() {
		return carDao.getAllCars();
	}

	@Override
	public Map<Integer,Car>  getCarMap(){
		List<Car> carList = carDao.getAllCars();
		return carList.stream().collect(Collectors.toMap(Car::getCarId, car -> car));
	}
}
