package com.slavon.service;

import com.slavon.entity.Specialization;

import java.util.List;

public interface SpecializationService {

    void insertOrUpdateSpecialization(Specialization specialization);

    Specialization getSpecializationById(int id);

    void deleteSpecialization(Specialization specialization);

    List<Specialization> getAllSpecialization();
}
