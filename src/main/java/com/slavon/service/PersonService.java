package com.slavon.service;

import com.slavon.entity.Person;

import java.util.List;
import java.util.Map;

public interface PersonService {

    void insertOrUpdatePerson(Person person);

    Person getPersonById(int id);

    Person getPersonByLogin(String login);

    void deletePerson(Person person);

    List<Person> getAllPeople();

    Map<Integer, Person> getPersonMap();

    List<Person> getClients();

    List<Person> getRepairers();
}
