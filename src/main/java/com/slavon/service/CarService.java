package com.slavon.service;

import com.slavon.entity.Car;

import java.util.List;
import java.util.Map;

public interface CarService {

    void insertOrUpdateCar(Car car);

    Car getCarById(int id);

    void deleteCar(Car car);

    List<Car> getAllCars();

    Map<Integer, Car> getCarMap();
}
