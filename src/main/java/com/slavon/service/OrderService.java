package com.slavon.service;

import com.slavon.entity.Order;
import com.slavon.entity.Person;

import java.util.List;
import java.util.Set;

public interface OrderService {

    void insertOrUpdateOrder(Order order);

    void deleteOrder(Order order);

    List<Order> getAllOrders();

    Order setPeopleForOrder(Order order, Set<Person> personSet);
}
