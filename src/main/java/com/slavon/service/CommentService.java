package com.slavon.service;

import com.slavon.entity.Comment;

import java.util.List;
import java.util.Map;


public interface CommentService {

	void insertComment(Comment comment);

	Comment getCommentById(int id);

	List<Comment> getAllComments();
	
	Map<Integer, Comment> getCommentMap();
}
