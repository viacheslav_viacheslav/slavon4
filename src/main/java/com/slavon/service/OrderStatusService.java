package com.slavon.service;

import com.slavon.entity.OrderStatus;

import java.util.List;
import java.util.Map;

public interface OrderStatusService {

    void insertOrUpdateOrderStatus(OrderStatus orderStatus);

    OrderStatus getOrderStatusById(int id);

    void deleteOrderStatus(OrderStatus orderStatus);

    List<OrderStatus> getAllOrderStatuses();

    Map<Integer, OrderStatus> gerOrderStatusMap();
}
