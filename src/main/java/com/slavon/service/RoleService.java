package com.slavon.service;

import com.slavon.entity.Role;

import java.util.List;

public interface RoleService {

    void insertOrUpdateRole(Role role);

    Role getRoleById(int id);

    void deleteRole(Role role);

    List<Role> getAllRoles();
}
