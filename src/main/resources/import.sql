INSERT INTO "ROLE" (role_name) VALUES ('ROLE_CLIENT'), ('ROLE_REPAIRER'), ('ROLE_ADMINISTRATOR'),('ROLE_BOSS');


INSERT INTO "PERSON"  (person_first_name, person_patronymic, person_last_name, person_email, person_telephone_number, person_login, person_password,person_role_id) VALUES ('Natalia','Vladimirovna','Baranova','nataha@yandex.ru',80631354654, 'nataha', 'nataha',1),('Vladislav','Oleksandrovich','Bezrukaviy','vladon@yandex.ru',8064323233, 'vlad', 'vlad',1),('Viacheslav','Gennadievych','Varianytsia','slavon@yandex.ru',8068746465, 'admin','admin',3),('Anastasija','Viktorovna','Vasil`eva','nastyha@yandex.ru',80684545646, 'nastyha', 'nastyha',1),('Ol`ga','Andreevna','Dolgova','olka@yandex.ru',8069365874, 'olka', 'olka',1),('Yulia','Aleksandrovna','Polovinkina','kastryla@yandex.ru',8067974466, 'kastryla','kastryla',1),('Aleksandr','Evgen`evych','Prince','prince@yandex.ru',8063135454, 'sanja', 'sanja',1),('Evgenyj','Vaganych','Petrosjan','petros@yandex.ru',80778717122, 'smeshok','smeshok',1),('Alexander', 'Alexandrovich', 'Mazurkevitch', 'mazur@yandex.ru', '846465464', 'mazur','mazur',2),('Irina', 'Alexandrovna', 'Skubach', 'skuba@yandex.ru', '895464322', 'irka','irka',2),('Sergey', 'Alexandrovich', 'Fast', 'fast@yandex.ru', '849363174', 'serij','serij',2),('Alexander', 'Glebovich', 'Afanasiev', 'afonja@yandex.ru', '8436433347', 'afonja','afonja',2),('Alexander', 'Alexandrovich', 'Bogaenko', 'boklaenych@yandex.ru', '8468544444','genka', 'genka',2),('Zhorik', NULL, 'Vartanov', 'zhorik@yandex.ru', '74864654332','zhorik', 'zhorik',2),('Dima', NULL, 'Petrov', 'dimon@yandex.ru', '8825155442','dimon', 'dimon',2),('boss', NULL, NULL, NULL, NULL,'boss', 'boss',4);

INSERT INTO "CAR"  (car_number, car_model, car_colour, car_description) VALUES ('XA 2347', 'opel', 'black', 'norm car'), ('XA 3446 BT', 'wolksvagen', 'blue', 'very good car'),('8754 XA', 'bmw', 'black', 'perfect car'),('SLAVON', 'vaz-2109', 'cherry', 'the best car ever'),('BB 457 OH', 'bugatti', 'black', 'koryto'),('BT 2387', 'lexus', 'yellow',''),('MT 234 YY', 'bmw', 'black', 'car was hit');

INSERT INTO "PERSON_CAR" (person_id, car_id) VALUES (5,4),(5,3),(3,5),(6,5),(8,6),(6,1),(7,7);


INSERT INTO "ORDER_STATUS" (order_status_id, order_status) VALUES (1, 'REGISTRATION'),(2, 'SELECTIVE DIAGNOSTICS'),(3, 'REPAIRER SUMMARY'),(4, 'CLIENT`S APPROVAL'),(5, 'REPAIR'),(6, 'ADDITIONAL PROBLEMS DETECTED'),(7, 'REPAIR COMPLETED'),(8, 'TRANSFER TO CLIENT'),(9, 'ORDER CLOSED'),(10, 'COMPLETE DIAGNOSTICS'),(11, 'ELECTRICIAN SUMMARY'),(12, 'CARCASE REPAIRER SUMMARY'),(13, 'ENGINE REPAIRER SUMMARY'),(14, 'CHASSIS REPAIRER SUMMARY'),(15, 'ELECTRICITY REPAIR'),(16, 'CARCASE REPAIR'),(17, 'ENGINE REPAIR'),(18, 'CHASSIS REPAIR');


INSERT INTO "SPECIALIZATION" (specialization, specialization_comments) VALUES('ELECTRICIAN',''),('CARCASE REPAIRER', ''),('ENGINE REPAIRER', 'beard'),('CHASSIS REPAIRER', '');


INSERT INTO "REPAIRER_SPECIALIZATION" (person_id, specialization_id) VALUES (9,1),(9,3),(10,2),(11,3),(12,2),(13,4);


INSERT INTO "ORDERS" (car_id, order_status_id, order_price) VALUES (4, 1, 100),(4, 4, 1000),(5, 9, 1000),(6, 9, 818);


INSERT INTO "COMMENT" (person_id,comment_date_time, comment_text, order_id) VALUES (3,parsedatetime('17-09-2008 18:47:52.690', 'dd-MM-yyyy hh:mm:ss.SS'), 'pochinite moj tazik',1),(14,parsedatetime('01-01-2009 20:47:52.690', 'dd-MM-yyyy hh:mm:ss.SS'), 'yeee brat prosti, pachinim atvichaju!',1),(3,parsedatetime('01-02-2009 18:47:52.690', 'dd-MM-yyyy hh:mm:ss.SS'), 'ny chto tam, ja yzhe zamahalsja na tramvae ezdit`!!',1),(14,parsedatetime('01-01-2010 18:47:52.690', 'dd-MM-yyyy hh:mm:ss.SS'), 'bratan, sezon mandarin bil, atvichajy ne so zla, zabili prosto',1),(3,parsedatetime('01-01-2016 18:47:52.690', 'dd-MM-yyyy hh:mm:ss.SS'), 'ny kak tam?',1),(14,parsedatetime('02-01-2016 18:47:52.690', 'dd-MM-yyyy hh:mm:ss.SS'), 'vse , po krasote bratan, yzhe naznacili mastera',1),(12,parsedatetime('03-01-2016 18:47:52.690', 'dd-MM-yyyy hh:mm:ss.SS'), 'ja ne bydy chinit` eto korito!!',1),(14,parsedatetime('02-02-2016 18:47:52.690', 'dd-MM-yyyy hh:mm:ss.SS'), 'eeee, yasja, ne vipendrivajsja',1),(11,parsedatetime('03-02-2016 18:47:52.690', 'dd-MM-yyyy hh:mm:ss.SS'), 'trebyetsa zamena masla',2),(15,parsedatetime('03-02-2016 20:47:52.690', 'dd-MM-yyyy hh:mm:ss.SS'), 'klient otkazalsja ot remonta',2),(6,parsedatetime('03-02-2016 21:47:52.690', 'dd-MM-yyyy hh:mm:ss.SS'), 'ploxo pokrasili krilo',3),(10,parsedatetime('03-02-2016 22:47:52.690', 'dd-MM-yyyy hh:mm:ss.SS'), 'v boxe bila slishkom malen`kaja temperatura',3),(5,parsedatetime('04-02-2016 10:47:52.690', 'dd-MM-yyyy hh:mm:ss.SS'), 'xoroshaja rabota, spasibo',4),(15,parsedatetime('10-09-2016 18:47:52.690', 'dd-MM-yyyy hh:mm:ss.SS'), 'vse peredelaem',3);


INSERT INTO "ORDER_PERSON" (order_id, person_id) VALUES (1, 4),(2, 3), (3, 2),(3,10),(2,11);




