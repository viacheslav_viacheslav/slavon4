package com.slavon.dao;

import static org.junit.Assert.assertEquals;

import com.slavon.entity.Comment;
import com.slavon.entity.Order;
import com.slavon.entity.Person;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/test_spring_config.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class CommentDaoImplSpringTest {

	@Autowired
	private CommentDao commentDao;

	protected IDatabaseTester tester;
	
	@Before
	public void setUp() throws Exception {
		tester = new JdbcDatabaseTester("org.h2.Driver", "jdbc:h2:mem:test_spring2", "root", "root");
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testInsertComment() throws Exception {
		Comment comment = new Comment();
		Person person = new Person("Ivan", "Ivanovich", "Ivanov", null,
				null, "ivan", "ivan",
				null, null, null,
				null, null);
		person.setPersonId(10);
		comment.setPerson(person);
		comment.setCommentText("Test comment text");
		Order order = new Order();
		order.setOrderId(3);
		comment.setOrderId(order);

		commentDao.insertComment(comment);

		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("COMMENT");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/comments/expected_comment_insert.xml"));
		ITable expectedTable = expectedDataSet.getTable("COMMENT");

		String[] ignore = { "comment_id","comment_date_time" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}
	
	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testGetCommentById() {
		Comment comment = commentDao.getCommentById(1);
		assertEquals(3, comment.getPerson().getPersonId());
		assertEquals("pochinite moj tazik", comment.getCommentText());
		assertEquals(1, comment.getOrder().getOrderId());
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testGetAllComments() {
		List<Comment> commentsActual = commentDao.getAllComments();
		List<Comment> commentsExpected = new ArrayList<>();
		Comment comment1 = new Comment();
		Person person = new Person("Ivan", "Ivanovich", "Ivanov", null,
				null, "ivan", "ivan",
				null, null, null,
				null, null);
		person.setPersonId(3);
		comment1.setPerson(person);
		comment1.setCommentText("pochinite moj tazik");
		Order order = new Order();
		order.setOrderId(1);
		comment1.setOrderId(order);
		comment1.setCommentId(1);
		commentsExpected.add(comment1);

		Comment comment2 = new Comment();
		Person person2 = new Person("Ivan", "Ivanovich", "Ivanov", null,
				null, "ivan", "ivan",
				null, null, null,
				null, null);
		person2.setPersonId(14);
		comment2.setPerson(person2);
		comment2.setCommentText("yeee brat prosti, pachinim atvichaju!");
		comment2.setOrderId(order);
		comment2.setCommentId(2);
		commentsExpected.add(comment2);

		Comment comment3 = new Comment();
		comment3.setPerson(person);
		comment3.setCommentText("ny chto tam, ja yzhe zamahalsja na tramvae ezdit`!!");
		comment3.setOrderId(order);
		comment3.setCommentId(3);
		commentsExpected.add(comment3);


		for (int i = 0; i < commentsExpected.size(); i++) {
			assertEquals(commentsExpected.get(i).getCommentId(), commentsActual.get(i).getCommentId());
			assertEquals(commentsExpected.get(i).getCommentText(), commentsActual.get(i).getCommentText());
			assertEquals(commentsExpected.get(i).getOrder(), commentsActual.get(i).getOrder());
			assertEquals(commentsExpected.get(i).getPerson(), commentsActual.get(i).getPerson());
		}
	}

}
