package com.slavon.dao;

import static org.junit.Assert.assertEquals;

import com.slavon.entity.Role;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/test_spring_config.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class RoleDaoImplSpringTest {

	@Autowired
	private RoleDao roleDao;

	protected IDatabaseTester tester;
	
	@Before
	public void setUp() throws Exception {
		tester = new JdbcDatabaseTester("org.h2.Driver", "jdbc:h2:mem:test_spring2", "root", "root");
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testInsertRole() throws Exception {
		Role role = new Role();
		role.setRoleName("ROLE_TEST");
		roleDao.insertOrUpdateRole(role);

		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("ROLE");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/roles/expected_role_insert.xml"));
		ITable expectedTable = expectedDataSet.getTable("ROLE");

		String[] ignore = { "role_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}
	
	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testGetRoleById() throws Exception {
		Role role = roleDao.getRoleById(1);
		assertEquals("CLIENT", role.getRoleName());
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testUpdateRole() throws Exception {
		Role role = roleDao.getRoleById(1);
		role.setRoleName("ROLE_MONKEY");
		roleDao.insertOrUpdateRole(role);

		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("ROLE");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/roles/expected_role_update.xml"));
		ITable expectedTable = expectedDataSet.getTable("ROLE");

		String[] ignore = { "role_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testDeleteRole() throws Exception {
		Role role = roleDao.getRoleById(1);
		roleDao.deleteRole(role);
		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("ROLE");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/roles/expected_role_delete.xml"));
		ITable expectedTable = expectedDataSet.getTable("ROLE");

		String[] ignore = { "role_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testGetAllRoles() {
		List<Role> rolesActual = roleDao.getAllRoles();
		List<Role> rolesExpected = new ArrayList<>();
		Role role1 = new Role("CLIENT",null);
		rolesExpected.add(role1);
		Role role2 = new Role("REPAIRER", null);
		rolesExpected.add(role2);
		Role role3 = new Role("ADMINISTRATOR", null);
		rolesExpected.add(role3);
		Role role4 = new Role("BOSS", null);
		rolesExpected.add(role4);

		for (int i = 0; i < rolesActual.size(); i++) {
			assertEquals(rolesExpected.get(i).getRoleName(), rolesActual.get(i).getRoleName());
		}
	}
}
