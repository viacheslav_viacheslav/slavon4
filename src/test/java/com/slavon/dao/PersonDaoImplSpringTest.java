package com.slavon.dao;

import static org.junit.Assert.assertEquals;

import com.slavon.entity.Person;
import com.slavon.entity.Role;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/test_spring_config.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class PersonDaoImplSpringTest {

	@Autowired
	private PersonDao personDao;

	protected IDatabaseTester tester;
	
	@Before
	public void setUp() throws Exception {
		tester = new JdbcDatabaseTester("org.h2.Driver", "jdbc:h2:mem:test_spring2", "root", "root");
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testInsertPerson() throws Exception {
		Person person = new Person();
		person.setFirstName("Ilon");
		person.setPersonPatronymic("Ivanovich");
		person.setPersonLastName("Mask");
		Role role = new Role();
		role.setRoleId(1);
		person.setPersonRole(role);
		person.setPersonStatus("ACTIVE");
		person.setPersonEmail("mask@yandex.ru");
		person.setPersonLogin("mask");
		person.setPersonPassword("mask");
		person.setPersonTelephoneNumber("888888");
		personDao.insertOrUpdatePerson(person);

		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("PERSON");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/people/expected_person_insert.xml"));
		ITable expectedTable = expectedDataSet.getTable("PERSON");

		String[] ignore = { "person_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}
	
	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testGetPersonById() {
		Person person = personDao.getPersonById(3);
		assertEquals("Viacheslav", person.getFirstName());
		assertEquals("Gennadievych", person.getPersonPatronymic());
		assertEquals("Varianytsia", person.getPersonLastName());
		assertEquals("slavon@yandex.ru", person.getPersonEmail());
		assertEquals("8068746465", person.getPersonTelephoneNumber());
		assertEquals("slavon", person.getPersonLogin());
		assertEquals("slavon", person.getPersonPassword());
		assertEquals("ACTIVE", person.getPersonStatus());
		assertEquals("ADMINISTRATOR", person.getPersonRole().getRoleName());
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testUpdatePerson() throws Exception {
		Person person = personDao.getPersonById(3);
		person.setFirstName("Arnold");
		person.setPersonPatronymic("Schwarzennegerovich");
		person.setPersonLastName("Stallone");
		person.setPersonEmail("razboinik@mail.ru");
		person.setPersonTelephoneNumber("2348759207");
		person.setPersonLogin("terminator");
		person.setPersonPassword("terminator");
		person.setPersonStatus("ACTIVE");
		person.setPersonEmail("razboinik@mail.ru");
		Role role = new Role();
		role.setRoleId(1);
		person.setPersonRole(role);
		personDao.insertOrUpdatePerson(person);

		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("PERSON");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/people/expected_person_update.xml"));
		ITable expectedTable = expectedDataSet.getTable("PERSON");

		String[] ignore = { "person_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testDeletePerson() throws Exception {
		Person person = personDao.getPersonById(3);
		personDao.deletePerson(person);
		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("PERSON");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/people/expected_person_delete.xml"));
		ITable expectedTable = expectedDataSet.getTable("PERSON");

		String[] ignore = { "person_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testGetAllPersons() {
		List<Person> personsActual = personDao.getAllPeople();
		List<Person> personsExpected = new ArrayList<>();
		Person person1 = new Person("Viacheslav","Gennadievych","Varianytsia","slavon@yandex.ru","8068746465","slavon","slavon",null,null,null,null,null);
		personsExpected.add(person1);
		Person person2 = new Person("Anastasija", "Viktorovna", "Vasil`eva", "nastyha@yandex.ru", "80684545646", "nastyha", "nastyha",null,null,null,null,null);
		personsExpected.add(person2);
		Person person3 = new Person("Evgenyj", "Andreevna", "Dolgova", "olka@yandex.ru", "8069365874", "olka", "olka",null,null,null,null,null);
		personsExpected.add(person3);

		for (int i = 0; i < personsExpected.size(); i++) {
			assertEquals(personsExpected.get(i).getFirstName(), personsActual.get(i).getFirstName());
			assertEquals(personsExpected.get(i).getPersonPatronymic(), personsActual.get(i).getPersonPatronymic());
			assertEquals(personsExpected.get(i).getPersonLastName(), personsActual.get(i).getPersonLastName());
			assertEquals(personsExpected.get(i).getPersonEmail(), personsActual.get(i).getPersonEmail());
			assertEquals(personsExpected.get(i).getPersonTelephoneNumber(), personsActual.get(i).getPersonTelephoneNumber());
			assertEquals(personsExpected.get(i).getPersonLogin(), personsActual.get(i).getPersonLogin());
			assertEquals(personsExpected.get(i).getPersonPassword(), personsActual.get(i).getPersonPassword());
		}
	}
}
