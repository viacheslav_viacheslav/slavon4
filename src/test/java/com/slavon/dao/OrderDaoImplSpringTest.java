package com.slavon.dao;

import static org.junit.Assert.assertEquals;

import com.slavon.entity.Car;
import com.slavon.entity.Order;
import com.slavon.entity.OrderStatus;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/test_spring_config.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class OrderDaoImplSpringTest {

	@Autowired
	private OrderDao orderDao;

	protected IDatabaseTester tester;
	
	@Before
	public void setUp() throws Exception {
		tester = new JdbcDatabaseTester("org.h2.Driver", "jdbc:h2:mem:test_spring2", "root", "root");
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testInsertOrder() throws Exception {
		Order order = new Order();
		OrderStatus orderStatus = new OrderStatus();
		orderStatus.setOrderStatusId(2);
		order.setOrderStatus(orderStatus);
		Car car = new Car();
		car.setCarId(3);
		order.setCar(car);
		order.setOrderPrice(858);
		orderDao.insertOrUpdateOrder(order);

		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("ORDERS");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/orders/expected_order_insert.xml"));
		ITable expectedTable = expectedDataSet.getTable("ORDERS");

		String[] ignore = { "order_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}
	
	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testGetOrderById() {
		Order order = orderDao.getOrderById(1);
		assertEquals(1, order.getOrderId());
		assertEquals(4, order.getCar().getCarId());
		assertEquals(1, order.getOrderStatus().getOrderStatusId());
		assertEquals(100F, order.getOrderPrice(),0);
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testUpdateOrder() throws Exception {
		Order order = orderDao.getOrderById(3);
		OrderStatus orderStatus = new OrderStatus();
		orderStatus.setOrderStatusId(2);
		order.setOrderStatus(orderStatus);
		Car car = new Car();
		car.setCarId(3);
		order.setCar(car);
		order.setOrderPrice(858);
		orderDao.insertOrUpdateOrder(order);

		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("ORDERS");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/orders/expected_order_update.xml"));
		ITable expectedTable = expectedDataSet.getTable("ORDERS");

		String[] ignore = { "order_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testDeleteOrder() throws Exception {
		Order order = orderDao.getOrderById(1);
		orderDao.deleteOrder(order);
		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("ORDERS");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/orders/expected_order_delete.xml"));
		ITable expectedTable = expectedDataSet.getTable("ORDERS");

		String[] ignore = { "order_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testGetAllOrders() {
		List<Order> ordersActual = orderDao.getAllOrders();
		List<Order> ordersExpected = new ArrayList<>();

		Order order1 = new Order();
		OrderStatus orderStatus = new OrderStatus();
		orderStatus.setOrderStatusId(1);
		order1.setOrderStatus(orderStatus);
		Car car1 = new Car();
		car1.setCarId(4);
		order1.setCar(car1);
		order1.setOrderPrice(100);
		ordersExpected.add(order1);

		Order order2 = new Order();
		OrderStatus orderStatus2 = new OrderStatus();
		orderStatus2.setOrderStatusId(4);
		order2.setOrderStatus(orderStatus2);
		Car car2 = new Car();
		car2.setCarId(4);
		order2.setCar(car2);
		order2.setOrderPrice(1000);
		ordersExpected.add(order2);

		Order order3 = new Order();
		OrderStatus orderStatus3 = new OrderStatus();
		orderStatus3.setOrderStatusId(9);
		order3.setOrderStatus(orderStatus3);
		Car car3 = new Car();
		car3.setCarId(5);
		order3.setCar(car3);
		order3.setOrderPrice(1000);
		ordersExpected.add(order3);

		for (int i = 0; i < ordersExpected.size(); i++) {
			assertEquals(ordersExpected.get(i).getOrderStatus().getOrderStatusId(), ordersActual.get(i).getOrderStatus().getOrderStatusId());
			assertEquals(ordersExpected.get(i).getOrderPrice(), ordersActual.get(i).getOrderPrice(),0);
			assertEquals(ordersExpected.get(i).getCar().getCarId(), ordersActual.get(i).getCar().getCarId());
		}
	}
}
