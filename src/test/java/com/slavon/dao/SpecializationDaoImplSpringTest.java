package com.slavon.dao;

import static org.junit.Assert.assertEquals;

import com.slavon.entity.Specialization;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/test_spring_config.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class SpecializationDaoImplSpringTest {

	@Autowired
	private SpecializationDao specializationDao;

	protected IDatabaseTester tester;
	
	@Before
	public void setUp() throws Exception {
		tester = new JdbcDatabaseTester("org.h2.Driver", "jdbc:h2:mem:test_spring2", "root", "root");
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testInsertSpecialization() throws Exception {
		Specialization specialization = new Specialization("TEST SPECIALIZATION","test comment",null);
		specializationDao.insertOrUpdateSpecialization(specialization);

		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("SPECIALIZATION");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/specializations/expected_specialization_insert.xml"));
		ITable expectedTable = expectedDataSet.getTable("SPECIALIZATION");

		String[] ignore = { "specialization_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}
	
	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testGetSpecializationById() throws Exception {
		Specialization specialization = specializationDao.getSpecializationById(1);
		assertEquals("ELECTRICIAN", specialization.getSpecializationName());
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testUpdateSpecialization() throws Exception {
		Specialization specialization = specializationDao.getSpecializationById(1);
		specialization.setSpecializationName("PROCRASTINATOR");
		specialization.setSpecializationComments("test beard");
		specializationDao.insertOrUpdateSpecialization(specialization);

		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("SPECIALIZATION");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/specializations/expected_specialization_update.xml"));
		ITable expectedTable = expectedDataSet.getTable("SPECIALIZATION");

		String[] ignore = { "specialization_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testDeleteSpecialization() throws Exception {
		Specialization specialization = specializationDao.getSpecializationById(3);
		specializationDao.deleteSpecialization(specialization);
		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("SPECIALIZATION");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/specializations/expected_specialization_delete.xml"));
		ITable expectedTable = expectedDataSet.getTable("SPECIALIZATION");

		String[] ignore = { "specialization_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testGetAllSpecializations() {
		List<Specialization> specializationsActual = specializationDao.getAllSpecialization();
		List<Specialization> specializationsExpected = new ArrayList<>();
		Specialization specialization1 = new Specialization("ELECTRICIAN","beard", null);
		specializationsExpected.add(specialization1);
		Specialization specialization2 = new Specialization("CARCASE REPAIRER",null, null);
		specializationsExpected.add(specialization2);
		Specialization specialization3 = new Specialization("ENGINE REPAIRER", "beard", null);
		specializationsExpected.add(specialization3);

		for (int i = 0; i < specializationsExpected.size(); i++) {
			assertEquals(specializationsExpected.get(i).getSpecializationName(), specializationsActual.get(i).getSpecializationName());
		}
	}
}
