package com.slavon.dao;

import static org.junit.Assert.assertEquals;

import com.slavon.entity.Car;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/test_spring_config.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class CarDaoImplSpringTest {

	@Autowired
	private CarDao carDao;

	protected IDatabaseTester tester;
	
	@Before
	public void setUp() throws Exception {
		tester = new JdbcDatabaseTester("org.h2.Driver", "jdbc:h2:mem:test_spring2", "root", "root");
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testInsertCar() throws Exception {
		Car car = new Car();
		car.setCarNumber("QQ 1367 AA");
		car.setCarModel("Audi");
		car.setColour("grey");
		car.setDescription("not such bad car");
		carDao.insertOrUpdateCar(car);

		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("CAR");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/cars/expected_car_insert.xml"));
		ITable expectedTable = expectedDataSet.getTable("CAR");

		String[] ignore = { "car_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}
	
	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testGetCarById() throws Exception {
		Car car = carDao.getCarById(1);
		assertEquals("XA 2347", car.getCarNumber());
		assertEquals("opel", car.getCarModel());
		assertEquals("black", car.getColour());
		assertEquals("norm car", car.getDescription());
	}
	
	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testUpdateCar() throws Exception {
		Car car = carDao.getCarById(1);
		car.setCarNumber("WW 3465");
		car.setCarModel("Honda");
		car.setColour("grey");
		car.setDescription("not bad car");
		carDao.insertOrUpdateCar(car);

		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("CAR");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/cars/expected_car_update.xml"));
		ITable expectedTable = expectedDataSet.getTable("CAR");

		String[] ignore = { "car_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}
	
	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testDeleteCar() throws Exception {
		Car car = carDao.getCarById(1);
		carDao.deleteCar(car);
		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("CAR");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/cars/expected_car_delete.xml"));
		ITable expectedTable = expectedDataSet.getTable("CAR");

		String[] ignore = { "car_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}
	
	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testGetAllCars() {
		List<Car> carsActual = carDao.getAllCars();
		List<Car> carsExpected = new ArrayList<>();
		Car car1 = new Car(1, "XA 2347", "opel", "black", "norm car", null, null);
		carsExpected.add(car1);
		Car car2 = new Car(2, "XA 3446 BT", "wolksvagen", "blue", "very good car", null, null);
		carsExpected.add(car2);
		Car car3 = new Car(3, "8754 XA", "bmw", "black", "perfect car", null, null);
		carsExpected.add(car3);
		Car car4 = new Car(4, "SLAVON", "vaz-2109", "cherry", "the best car ever", null, null);
		carsExpected.add(car4);
		Car car5 = new Car(5, "BB 457 OH", "bugatti", "black", "koryto", null, null);
		carsExpected.add(car5);
		Car car6 = new Car(6, "BT 2387", "lexus", "yellow", null, null, null);
		carsExpected.add(car6);
		Car car7 = new Car(7, "MT 234 YY", "bmw", "black", "car was hit", null, null);
		carsExpected.add(car7);

		for (int i = 0; i < carsActual.size(); i++) {
			assertEquals(carsExpected.get(i).getCarModel(), carsActual.get(i).getCarModel());
			assertEquals(carsExpected.get(i).getCarNumber(), carsActual.get(i).getCarNumber());
			assertEquals(carsExpected.get(i).getColour(), carsActual.get(i).getColour());
			assertEquals(carsExpected.get(i).getDescription(), carsActual.get(i).getDescription());
		}
	}
}
