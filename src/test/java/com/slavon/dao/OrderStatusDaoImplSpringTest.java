package com.slavon.dao;

import static org.junit.Assert.assertEquals;

import com.slavon.entity.Order;
import com.slavon.entity.OrderStatus;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/test_spring_config.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class OrderStatusDaoImplSpringTest {

	@Autowired
	private OrderStatusDao orderStatusDao;

	protected IDatabaseTester tester;
	
	@Before
	public void setUp() throws Exception {
		tester = new JdbcDatabaseTester("org.h2.Driver", "jdbc:h2:mem:test_spring2", "root", "root");
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testInsertOrderStatus() throws Exception {
		Order order = new Order();
		order.setOrderId(5);
		Set<Order> orderSet = new HashSet<>();
		orderSet.add(order);
		OrderStatus orderStatus = new OrderStatus("TEST ORDER STATUS",orderSet);
		orderStatusDao.insertOrUpdateOrderStatus(orderStatus);

		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("ORDER_STATUS");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/orderStatuses/expected_order_status_insert.xml"));
		ITable expectedTable = expectedDataSet.getTable("ORDER_STATUS");

		String[] ignore = { "order_status_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}
	
	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testGetOrderStatusById() throws Exception {
		OrderStatus orderStatus = orderStatusDao.getOrderStatusById(1);
		assertEquals("REGISTRATION", orderStatus.getOrderStatus());
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testUpdateOrderStatus() throws Exception {
		OrderStatus orderStatus = orderStatusDao.getOrderStatusById(1);
		orderStatus.setOrderStatus("TESTREGISTRATION");
		orderStatusDao.insertOrUpdateOrderStatus(orderStatus);

		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("ORDER_STATUS");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/orderStatuses/expected_order_status_update.xml"));
		ITable expectedTable = expectedDataSet.getTable("ORDER_STATUS");

		String[] ignore = { "orderStatus_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testDeleteOrderStatus() throws Exception {
		OrderStatus orderStatus = orderStatusDao.getOrderStatusById(1);
		orderStatusDao.deleteOrderStatus(orderStatus);
		IDataSet iDataSet = tester.getConnection().createDataSet();
		ITable actualTable = iDataSet.getTable("ORDER_STATUS");

		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new File("src/test/resources/orderStatuses/expected_order_status_delete.xml"));
		ITable expectedTable = expectedDataSet.getTable("ORDER_STATUS");

		String[] ignore = { "orderStatus_id" };
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignore);
	}

	@Test
	@DatabaseSetup("/DBCopy.xml")
	public void testGetAllOrderStatuss() {
		List<OrderStatus> orderStatusesActual = orderStatusDao.getAllOrderStatuses();
		List<OrderStatus> orderStatusesExpected = new ArrayList<>();
		OrderStatus orderStatus1 = new OrderStatus("REGISTRATION", null);
		orderStatusesExpected.add(orderStatus1);
		OrderStatus orderStatus2 = new OrderStatus("SELECTIVE DIAGNOSTICS", null);
		orderStatusesExpected.add(orderStatus2);
		OrderStatus orderStatus3 = new OrderStatus("REPAIRER SUMMARY", null);
		orderStatusesExpected.add(orderStatus3);

		for (int i = 0; i < orderStatusesExpected.size(); i++) {
			assertEquals(orderStatusesExpected.get(i).getOrderStatus(), orderStatusesActual.get(i).getOrderStatus());
		}
	}
}
